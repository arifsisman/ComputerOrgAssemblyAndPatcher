INCLUDE Irvine32.inc
INCLUDE macros.inc

BUFFER_SIZE = 5000

.data
buffer		BYTE	BUFFER_SIZE DUP(?)
filename	BYTE	"newfile.txt",0
fileHandle  HANDLE ?
key         DWORD  ?
pPrompt     BYTE    "Enter a Password Between [1-255]: ", 0
error       BYTE    "The password must be within 1 - 255!", 0

.data?
len			DWORD	?

.code
main PROC

; Open the file for input.
	mov		edx,OFFSET filename
	call	OpenInputFile
	mov		fileHandle,eax

; Check for errors.
	cmp		eax,INVALID_HANDLE_VALUE		; error opening file?
	jne		file_ok					; no: skip
	mWrite	<"Cannot open file",0dh,0ah>
	jmp		quit						; and quit
file_ok:

; Read the file into a buffer.
	mov		edx,OFFSET buffer
	mov		ecx,BUFFER_SIZE
	call	ReadFromFile
	jnc		check_buffer_size			; error reading?
	mWrite	"Error reading file. "		; yes: show error message
	call	WriteWindowsMsg
	jmp		close_file
	
check_buffer_size:
	cmp		eax,BUFFER_SIZE				; buffer large enough?
	jb		buf_size_ok					; yes
	mWrite	<"Error: Buffer too small for the file",0dh,0ah>
	jmp		quit						; and quit
	
buf_size_ok:	
	mov		buffer[eax],0		; insert null terminator
	mWrite	"File size: "
	call	WriteDec			; display file size
	MOV		len,eax
	call	Crlf

; Display the buffer.
	mWrite	<"Buffer:",0dh,0ah>
	mov		edx,OFFSET buffer	; display the buffer
	call	WriteString
	call	Crlf

close_file:
	mov		eax,fileHandle
	call	CloseFile

	call    InputThePass                    ; input the security pass
	call	getkey

quit:
	exit
main ENDP

TranslateBuffer PROC
	sub len,3
    mov     ecx, len
    mov     eax, key
	add		eax,ebx
    xor     esi, esi
XorByte:
    xor     buffer[esi], al
    inc     esi
    cmp     esi, ecx
    jne     XorByte
    ret
TranslateBuffer ENDP

InputThePass PROC
PromptForKey:
    mov     edx, OFFSET pPrompt             ; display a prompt 
    call    WriteString                     ; Enter a private pass [1-255] 
    call    ReadInt                         ; read int into system 
    test    eax, eax
    jz      BadKey
    cmp     eax, 255
    jg      BadKey

    mov     key, eax
    ret

BadKey:
    mov     edx, OFFSET error               ; The pass must be within 1 - 255! 
    call    WriteString  
    call    Crlf
    jmp     PromptForKey
InputThePass ENDP

writekey PROC
	mWrite	<"Access Granted.",0dh,0ah>
	mWrite	<"Decrypted:",0dh,0ah>
	mov     edx, OFFSET buffer          ; display the buffer
    call    WriteString
	call    Crlf
	call	waitmsg
    ret
writekey ENDP

decrypt PROC
	call	TranslateBuffer
	call	writekey
    ret
decrypt ENDP

wrongkey PROC
	mWrite	<"Access Denied.",0dh,0ah>
	call	waitmsg
    ret
wrongkey ENDP

getkey PROC
		mov ecx, len        ;COUNTER FOR LOOP (LENGTH-1).
	sub	ecx,1
	mov esi, offset buffer  ;eSI POINTS TO FIRST WORD IN ARRAY.
	jjj:
	add esi,1				;POINT TO NEXT WORD IN ARRAY.    
	loop jjj				;eCX--, IF eCX > 0 REPEAT.
	mov eax, [ esi ]        ;KEY!

	cmp		key,eax		
	jne		wrongkey
	call	decrypt
	ret
getkey ENDP

END main