INCLUDE Irvine32.inc
includelib irvine32.lib
includelib kernel32.lib
includelib user32.lib
BUFMAX = 128                                ; maximum buffer size
BUFFER_SIZE = 5000

.data
sPrompt     BYTE    "Enter Username: ", 0
pPrompt     BYTE    "Enter a Password Between [1-255]: ", 0
filename	BYTE	"newfile.txt",0
fileHandle  HANDLE ?			; handle to output file
bytesWritten DWORD ?    			; number of bytes written
sEncrypt    BYTE    "Cypher text: ", 0
sDecrypt    BYTE    "Decrypted: ", 0
error       BYTE    "The password must be within 1 - 255!", 0
errMsg		BYTE	"Cannot open file",0dh,0ah,0
cr			BYTE	0dh,0ah

.data?
bufSize     DWORD   ?
key         DWORD   ?
bufFile     BYTE    BUFFER_SIZE DUP (?)
buffer      BYTE    BUFMAX + 1 DUP (?)


.code 
main PROC
    call    InputTheUser                    ; input the plain text
    call    InputThePass                    ; input the security pass

	call    TranslateBuffer                 ; encrypt the buffer

	mov     edx, offset sEncrypt
    call    WriteString

    mov     edx, OFFSET buffer              ; display encrypted message
    call    WriteString
    call    Crlf
	
	call    CypherFile 

    call    TranslateBuffer                 ; decrypt the buffer

    mov     edx, offset sDecrypt
    call    WriteString

    mov     edx, OFFSET buffer              ; display encrypted message
    call    WriteString
    call    Crlf

    call    WaitMsg
    exit

	main ENDP


InputTheUser PROC
    mov     edx, OFFSET sPrompt             ; display a prompt
    call    WriteString                     ; "Enter username"

    mov     ecx, BUFMAX                     ; maximum character count
    mov     edx, OFFSET buffer              ; point to the buffer
    call    ReadString                      ; input the string

    push    offset buffer
    call    Str_length
    mov     bufSize, eax                    ; save the length
    ret
InputTheUser ENDP

InputThePass PROC
PromptForKey:
    mov     edx, OFFSET pPrompt             ; display a prompt 
    call    WriteString                     ; Enter a private pass [1-255] 
    call    ReadInt                         ; read int into system 
    test    eax, eax
    jz      BadKey
    cmp     eax, 255
    jg      BadKey

    mov     key, eax
    ret

BadKey:
    mov     edx, OFFSET error               ; The pass must be within 1 - 255! 
    call    WriteString  
    call    Crlf
    jmp     PromptForKey
InputThePass ENDP

TranslateBuffer PROC
    mov     ecx, bufSize
    mov     eax, key
    xor     esi, esi
XorByte:
    xor     buffer[esi], al
    inc     esi
    cmp     esi, ecx	
    jne     XorByte
    ret
TranslateBuffer ENDP

CypherFile PROC 		
	;INVOKE CreateFile,
	  ;ADDR filename, GENERIC_WRITE, DO_NOT_SHARE, NULL,
	  ;OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0

	mov     edx, OFFSET fileName 
    call    CreateOutputFile 

	mov fileHandle,eax			; save file handle
	.IF eax == INVALID_HANDLE_VALUE
	  mov  edx,OFFSET errMsg		; Display error message
	  call WriteString
	  exit
	.ENDIF

	; Move the file pointer to the end of the file
	INVOKE SetFilePointer,
	  fileHandle,0,0,FILE_END

	; Append text to the file
	INVOKE WriteFile,
	fileHandle, ADDR buffer, bufSize,
	ADDR bytesWritten, 0

	INVOKE WriteFile,
	fileHandle, ADDR cr,2,
	ADDR bytesWritten, 0

	INVOKE WriteFile,
	fileHandle,ADDR key, 1,
	ADDR bytesWritten, 0

	INVOKE CloseHandle, fileHandle
CypherFile ENDP

DisplayMessage PROC
    mov     edx, OFFSET buffer          ; display the buffer
    call    WriteString
    call    Crlf
    call    Crlf
    ret
DisplayMessage ENDP
END main