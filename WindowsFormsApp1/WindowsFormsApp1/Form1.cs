﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string KlasorYolu,str,KayitYolu="D:\\HW2Complete.exe";
        public OpenFileDialog Klasor = new OpenFileDialog();
        private void textBox1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                str = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
            }
            textBox1.Text = str;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Contains("HW2.exe") && textBox2.Text.Contains(".exe"))
                Crack();
            else
                MessageBox.Show("Path geçersiz.","Uyarı",MessageBoxButtons.OK,MessageBoxIcon.Error);
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Executable File|*.exe";
            save.OverwritePrompt = true;
            save.CreatePrompt = true;

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter Kayit = new StreamWriter(save.FileName);
                Kayit.WriteLine(textBox1.Text);
                Kayit.Close();
                KayitYolu = save.FileName;
                textBox2.Text = KayitYolu;
            }
        }

        public static void Crack()
        {
            byte[] buffer1 = File.ReadAllBytes(str);
            string base64Encoded = Convert.ToBase64String(buffer1);

            //byte[] buffer2 = File.ReadAllBytes(@"G:\CSE206 HW5 – 20160807012\HW2Cracked.exe");
            //string base64Encoded1 = Convert.ToBase64String(buffer2);

            //FarkliByteBul(buffer1, buffer2);

            buffer1 = Convert.FromBase64String(base64Encoded);
            buffer1[10926] = 144;
            buffer1[10927] = 144;
            File.WriteAllBytes(KayitYolu, buffer1);

            MessageBox.Show("Crack işlemi tamamlandı.","Başarılı");
        }

        public static void FarkliByteBul(byte[] a, byte[] b)
        {
            int[] array = new int[2];

            Console.WriteLine("Numbers in first array but not second array:");
            int c = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (b[i] != a[i])
                {
                    array[c] = i;
                    c++;
                }
            }
            for (int j = 0; j < array.Length; j++)
                Console.WriteLine("line " + array[j]);
        }
    }
}
